import { Component, OnInit } from '@angular/core';
import { Subject, debounceTime, switchMap } from 'rxjs';
import { UserService } from '../../services/user/user.service';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { User } from '../../models/user.model';
import { CapitalizeFirstWordPipe } from '../../pipes/capitalize-first-word/capitalize-first-word.pipe';
import { AppUserForm } from '../user-form/user-form.component';
import { AppBootstrap } from '../bootstrap/bootstrap.component';

@Component({
  selector: 'app-users',
  standalone: true,
  imports: [CommonModule, FormsModule, CapitalizeFirstWordPipe, AppUserForm, AppBootstrap],
  template: `
    <div style="margin: 20px;">
      Exercise 1
      <div style="margin: 20px;">
        <input type="text" [(ngModel)]="query" (ngModelChange)="querySubject.next($event)">
        <div *ngFor="let user of users">
            {{ user.email }}
        </div>
      </div>

      Exercise 2
      <div style="margin: 20px;">
        <div *ngFor="let user of users">
            {{ user.name | capitalizeFirstWord }}
        </div>
      </div>

      Exercise 3
      <div style="margin: 20px;">
        <app-user-form (event)="onUserSubmitted($event)"></app-user-form>
      </div>

      Exercise CSS & Bootstrap
      <div style="margin: 20px;">
        <app-bootstrap></app-bootstrap>
      </div>
    </div>
  `
})
export class AppUsers implements OnInit {

  query = '';
  querySubject = new Subject<string>();

  // users: { email: string; }[] = [];
  users: User[] = [];

  constructor(
    private userService: UserService
  ) {
  }

  ngOnInit(): void {

    // ORIGINAL
    // concat(
    //   of(this.query),
    //   this.querySubject.asObservable()
    // ).pipe(
    //   concatMap(q =>
    //     timer(0, 60000).pipe(
    //       this.userService.findUsers(q)
    //     )
    //   )
    // ).subscribe({
    //   next: (res: any) => this.users = res
    // });

    this.querySubject.pipe(
      debounceTime(300),
      switchMap(query => this.userService.findUsers(query))
    ).subscribe(users => this.users = users);

    // Trigger initial load with empty query
    this.querySubject.next(this.query);
  }

  /**
   * ORIGINAL
   * In order to improve performance I transformed this method into a pipe, since Angular only executes it when the value changes
   * otherwise the method would be called repeatelly during change detection.
   */
  // getCapitalizeFirstWord(name: string): string {
  //   return name.split(' ').map(n => n.substring(0, 1).toUpperCase() + n.substring(1).toLowerCase()).join(' ');
  // }

  onUserSubmitted($event: any) {
console.log($event);
  }
}
