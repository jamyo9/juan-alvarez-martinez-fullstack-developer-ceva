import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, ValidatorFn, Validators } from '@angular/forms';
import { User } from '../../models/user.model';

import { DateTime } from "luxon";


@Component({
  selector: 'app-user-form',
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule],
  template: `
    <form [formGroup]="userForm" (ngSubmit)="doSubmit()">
        <input type="text" placeholder="email" formControlName="email">
        <input type="text" placeholder="name" formControlName="name">
        <input type="date" placeholder="birthday" formControlName="birthday">
        <input type="number" placeholder="zip" formControlName="zip">
        <input type="text" placeholder="city" formControlName="city">

        <button type="submit" [disabled]="userForm.invalid">Submit</button>
    </form>
  `
})
export class AppUserForm {

  @Output() event = new EventEmitter<User>;

  userForm: FormGroup;
  
  constructor(
    private formBuilder: FormBuilder
  ) {
    this.userForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      name: ['', [Validators.required, Validators.maxLength(128)]],
      birthday: ['', [this.dateLessThanTodayValidator]],
      zip: ['', [Validators.required]],
      city: ['', [Validators.required, Validators.pattern(/^[a-zA-Z ]*$/)]]
    });
  }

  doSubmit(): void {
    if (this.userForm.valid) {
      const { email, name, birthday, zip, city } = this.userForm.value;
      const user: User = {
        email: email,
        name: name,
        birthday: birthday,
        address: {
          zip: zip,
          city: city
        }
      }
      this.event.emit(user);
    }
  }

  private dateLessThanTodayValidator: ValidatorFn = (): {
    [key: string]: any;
  } | null => {
    let invalid = false;
    const birthday = this.userForm && this.userForm.get("birthday")?.value;
    if (!!birthday) {
      invalid = !this.isBeforeToday(DateTime.fromISO(birthday));
    }
    return (invalid) ? { invalidRange: { birthday } } : null;
  };

  private isBeforeToday(date: DateTime): boolean {
    if(!!date) {
      return date.startOf('day').toMillis() < DateTime.now().startOf('day').toMillis();
    }

    return false;
  }
}
