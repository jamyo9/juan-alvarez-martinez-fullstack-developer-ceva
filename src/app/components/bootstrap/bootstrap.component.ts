import { Component } from '@angular/core';

@Component({
  selector: 'app-bootstrap',
  standalone: true,
  imports: [],
  templateUrl: './bootstrap.component.html'
})
export class AppBootstrap {

  unreadNotifications = '+123'

  constructor() { }
}
