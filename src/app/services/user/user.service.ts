import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from '../../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  constructor() { }

  private users: User[] = [
    {
      email: 'john.doe@example.com',
      name: 'john doe',
      birthday: new Date('1990-01-01'),
      address: {
        zip: 12345,
        city: 'New York'
      }
    },
    {
      email: 'jane.smith@example.com',
      name: 'jane smith',
      birthday: new Date('1985-05-15'),
      address: {
        zip: 54321,
        city: 'Los Angeles'
      }
    },
    {
      email: 'alice.jones@example.com',
      name: 'alice jones',
      address: {
        zip: 67890,
        city: 'Chicago'
      }
    },
    {
      email: 'bob.brown@example.com',
      name: 'bob brown',
      birthday: new Date('2000-12-30'),
      address: {
        zip: 98765,
        city: 'Houston'
      }
    }
  ];

  findUsers(query: string): Observable<User[]> {
    const lowerCaseQuery = query.toLowerCase();
    const filteredUsers = this.users.filter(user =>
      user.email.toLowerCase().includes(lowerCaseQuery) ||
      user.name.toLowerCase().includes(lowerCaseQuery) ||
      user.address.city.toLowerCase().includes(lowerCaseQuery)
    );
    return of(filteredUsers);
  }
}
