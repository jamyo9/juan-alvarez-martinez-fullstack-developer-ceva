import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizeFirstWord',
  standalone: true,
})
export class CapitalizeFirstWordPipe implements PipeTransform {
  transform(value: string): string {
    if(value == null) {
    	throw new Error('Failed to capitalize first word with null');
	}

	if (!value) {
      return value;
    }

    return value.split(' ').map(word => 
			word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()
		).join(' ');
  }
}