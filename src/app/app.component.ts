import { CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';
import { AppUsers } from './components/users/users.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [AppUsers],
  template: `<app-users></app-users>`,
  styleUrls: ['./app.component.css'],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppComponent {

}
