<p align="center">
  <h3 align="center">Juan Alvarez Martinez</h3>

  <p align="center">
    Full Stack Developer/CEVA
    <br>
    <br>
    Technical Test
    <br>
    <br>
    <a href="https://gitlab.com/gabriel-allaigre/exercices/-/wikis/Exercices/">Exercises</a>
  </p>
</p>

## Setup

```bash
npm intall
npm run start

In your browser go to http://localhost:6200/
```

## Description

This is a project created to solve all the Exercises exposed in the Technical Test provided [here](https://gitlab.com/gabriel-allaigre/exercices/-/wikis/Exercices/)

## NodeJs

### Exercise: Is there a problem? (1 points)

```bash
// Call web service and return count user, (got is library to call url)
async function getCountUsers() {
  return { total: await got.get('https://my-webservice.moveecar.com/users/count') };
}

// Add total from service with 20
async function computeResult() {
  const result = getCountUsers();
  return result.total + 20;
}
```

### Solution

In the function computeResult() it is missing the await key word in order to wait for the Promise to be complited:

```bash
// Call web service and return count user, (got is library to call url)
async function getCountUsers() {
  return { total: await got.get('https://my-webservice.moveecar.com/users/count') };
}

// Add total from service with 20
async function computeResult() {
  const result = await getCountUsers();
  return result.total + 20;
}
```

### Exercise: Is there a problem? (2 points)

```bash
// Call web service and return total vehicles, (got is library to call url)
async function getTotalVehicles() {
    return await got.get('https://my-webservice.moveecar.com/vehicles/total');
}

function getPlurial() {
    let total;
    getTotalVehicles().then(r => total = r);
    if (total <= 0) {
        return 'none';
    }
    if (total <= 10) {
        return 'few';
    }
    return 'many';
}
```

### Solution

Similar to the previous question, the total property is undefined in the if statements because the ifs are not waiting for the getTotalVehicles method to be completed. In order to solve this problem, I would add the async to the getPlural function, and an await to the call of getTotalVehicles:

```bash
// Call web service and return total vehicles, (got is library to call url)
async function getTotalVehicles() {
    return await got.get('https://my-webservice.moveecar.com/vehicles/total');
}

async function getPlurial() {
    let total;
    await getTotalVehicles().then(r => total = r);
    if (total <= 0) {
        return 'none';
    }
    if (total <= 10) {
        return 'few';
    }
    return 'many';
}
```

### Exercise: Unit test (2 points)

Write unit tests in jest for the function below in typescript

```bash
import { expect, test } from '@jest/globals';

function getCapitalizeFirstWord(name: string): string {
  if (name == null) {
    throw new Error('Failed to capitalize first word with null');
  }
  if (!name) {
    return name;
  }
  return name.split(' ').map(
    n => n.length > 1 ? (n.substring(0, 1).toUpperCase() + n.substring(1).toLowerCase()) : n
  ).join(' ');
}

test('1. test', async function () {
    ...
});
```

### Solution

```bash
import { expect, test } from '@jest/globals';

function getCapitalizeFirstWord(name: string): string {
  if (name == null) {
    throw new Error('Failed to capitalize first word with null');
  }
  if (!name) {
    return name;
  }
  return name.split(' ').map(
    n => n.length > 1 ? (n.substring(0, 1).toUpperCase() + n.substring(1).toLowerCase()) : n
  ).join(' ');
}

test('1. should throw an error when input is null', () => {
  expect(() => {
    getCapitalizeFirstWord(null);
  }).toThrow('Failed to capitalize first word with null');
});

test('2. should return an empty string when input is an empty string', () => {
  expect(getCapitalizeFirstWord('')).toBe('');
});

test('3. should capitalize the first letter of a single word', () => {
  expect(getCapitalizeFirstWord('hello')).toBe('Hello');
});

test('4. should capitalize the first letter of each word in a sentence', () => {
  expect(getCapitalizeFirstWord('hello world')).toBe('Hello World');
});

test('5. should not change single letter words', () => {
  expect(getCapitalizeFirstWord('a b c')).toBe('A B C');
});

test('6. should capitalize first letter and lower case the rest of each word', () => {
  expect(getCapitalizeFirstWord('heLLo wORld')).toBe('Hello World');
});

test('7. should handle strings with multiple spaces between words', () => {
  expect(getCapitalizeFirstWord('hello   world')).toBe('Hello   World');
});

test('8. should handle strings with punctuation correctly', () => {
  expect(getCapitalizeFirstWord('hello-world')).toBe('Hello-world');
});
```

## Angular

### Exercise: Is there a problem and improve the code (5 points)

```bash
@Component({
  selector: 'app-users',
  template: `
    <input type="text" [(ngModel)]="query" (ngModelChange)="querySubject.next($event)">
    <div *ngFor="let user of users">
        {{ user.email }}
    </div>
  `
})
export class AppUsers implements OnInit {

  query = '';
  querySubject = new Subject<string>();

  users: { email: string; }[] = [];

  constructor(
    private userService: UserService
  ) {
  }

  ngOnInit(): void {
    concat(
      of(this.query),
      this.querySubject.asObservable()
    ).pipe(
      concatMap(q =>
        timer(0, 60000).pipe(
          this.userService.findUsers(q)
        )
      )
    ).subscribe({
      next: (res) => this.users = res
    });
  }
}
```

### Solution

In this project it is the developed in the AppUsers Component as Exercise 1.

### Exercise: Improve performance (5 points)

```bash
@Component({
  selector: 'app-users',
  template: `
    <div *ngFor="let user of users">
        {{ getCapitalizeFirstWord(user.name) }}
    </div>
  `
})
export class AppUsers {

  @Input()
  users: { name: string; }[];

  constructor() {}

  getCapitalizeFirstWord(name: string): string {
    return name.split(' ').map(n => n.substring(0, 1).toUpperCase() + n.substring(1).toLowerCase()).join(' ');
  }
}
```

### Solution

In this project it is the developed in the AppUsers Component as Exercise 2.

### Exercise: Forms (8 points)

Complete and modify AppUserForm class to use Angular Reactive Forms. Add a button to submit.
The form should return data in this format

```bash
{
  email: string; // mandatory, must be a email
  name: string; // mandatory, max 128 characters
  birthday?: Date; // Not mandatory, must be less than today
  address: { // mandatory
    zip: number; // mandatory
    city: string; // mandatory, must contains only alpha uppercase and lower and space
  };
}
```

```bash
@Component({
  selector: 'app-user-form',
  template: `
    <form>
        <input type="text" placeholder="email">
        <input type="text" placeholder="name">
        <input type="date" placeholder="birthday">
        <input type="number" placeholder="zip">
        <input type="text" placeholder="city">
    </form>
  `
})
export class AppUserForm {

  @Output()
  event = new EventEmitter<{ email: string; name: string; birthday: Date; address: { zip: number; city: string; };}>;

  constructor(
    private formBuilder: FormBuilder
  ) {
  }

  doSubmit(): void {
    this.event.emit(...);
  }
}
```

### Solution

In this project it is the developed in the AppUsers Component as Exercise 3.

## CSS & Bootstrap

### Exercise: Card (5 points)

<img src="https://gitlab.com/gabriel-allaigre/exercices/-/wikis/uploads/0388377207d10f8732e1d64623a255b6/image.png" alt="Bootstrap exercise"/>

### Solution

In this project it is the developed in the AppUsers Component as Exercise CSS & Bootstrap.

## MongoDb

### Exercise: MongoDb request (3 points)

MongoDb collection users with schema

```bash
  {
    email: string;
    first_name: string;
    last_name: string;
    roles: string[];
    last_connection_date: Date;
  }
```

Complete the query, you have a variable that contains a piece of text to search for. Search by exact email, starts with first or last name and only users logged in for 6 months

```bash
db.collections('users').find(...);
```

What should be added to the collection so that the query is not slow?

### Solution

```bash
db.users.find({
  $and: [
    {
      $or: [
        { email: searchText }, // Exact email match
        { first_name: { $regex: `^${searchText}`, $options: 'i' } }, // First name starts with searchText (case insensitive)
        { last_name: { $regex: `^${searchText}`, $options: 'i' } } // Last name starts with searchText (case insensitive)
      ]
    },
    { last_connection_date: { $gte: new Date().setMonth(new Date().getMonth() - 6) } } // Users logged in within the last 6 months
  ]
});
```

In order to improve the performace of the DB, it will be needed to add indexes:

```bash
db.users.createIndex({ email: 1 }, { unique: true });
db.users.createIndex({ first_name: 1 });
db.users.createIndex({ last_name: 1 });
db.users.createIndex({ last_connection_date: 1 });
```

### Exercise: MongoDb aggregate (5 points)

MongoDb collection users with schema

```bash
  {
    email: string;
    first_name: string;
    last_name: string;
    roles: string[];
    last_connection_date: Date;
  }
```

Complete the aggregation so that it sends user emails by role ({\_id: 'role', users: [email,...]})

```bash
db.collections('users').aggregate(...);
```

### Solution

```bash
db.collections('users').aggregate([
  // Unwind the roles array so each document has one role
  { $unwind: "$roles" },

  // Group by role and accumulate emails into an array
  {
    $group: {
      _id: "$roles",
      users: { $push: "$email" }
    }
  },

  // Project the final output to match the specified format
  {
    $project: {
      _id: 0, // Exclude the default _id field
      role: "$_id", // Rename _id to role
      users: 1 // Include the users array
    }
  }
])
```

### Exercise: MongoDb update (5 points)

MongoDb collection users with schema

```bash
  {
    email: string;
    first_name: string;
    last_name: string;
    roles: string[];
    last_connection_date: Date;
    addresses: {
        zip: number;
        city: string;
    }[]:
  }
```

Update document ObjectId("5cd96d3ed5d3e20029627d4a"), modify only last_connection_date with current date

```bash
db.collections('users').updateOne(...);
```

Update document ObjectId("5cd96d3ed5d3e20029627d4a"), add a role admin

```bash
db.collections('users').updateOne(...);
```

Update document ObjectId("5cd96d3ed5d3e20029627d4a"), modify addresses with zip 75001 and replace city with Paris 1

```bash
db.collections('users').updateOne(...);
```

### Solution

```bash
db.users.updateOne(
  { _id: ObjectId("5cd96d3ed5d3e20029627d4a") },
  { $set: { last_connection_date: new Date() }}
)
```

```bash
db.users.updateOne(
  { _id: ObjectId("5cd96d3ed5d3e20029627d4a") },
  { $addToSet: { rol: 'admin' } }
)
```

```bash
db.users.updateOne(
  {_id: ObjectId("5cd96d3ed5d3e20029627d4a")},
  {
    $set: {
      'addresses.$[address].city': 'Paris 1'
    }
    }, {
      arrayFilters: [
        {'address.zip': 75001}
      ]
    })
```
